# DOCUMENT VARIABLES

NAME= article
TEXSRCS= header.tex
article_BIBTEXSRCS= bibliography.bib
CLEAN_FILES+= *symbols* images/*~ *.nav *.snm

# PROJECT VARIABLES

GZCAT= zcat
VIEWPDF= evince

include /usr/share/latex-mk/latex.gmk
