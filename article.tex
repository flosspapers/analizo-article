\input{header}
\usepackage{scalefnt}
\begin{document}

\maketitle

\begin{abstract}
  This paper describes Analizo, a free, multi-language, extensible
  source code analysis and visualization toolkit.
  %
  It supports the extraction and calculation of a fair number of source code
  metrics, generation of dependency graphs, and software evolution analysis.
\end{abstract}

\section{Introduction}

%The practice of Software Engineering requires understanding software,
%its main product, which can exhibit arbitrary complexity.
%%
Software engineers need to analyze and visualize the software they
create or maintain in order to better understand it. Software
Engineering researchers need to analyze software products in order to
draw conclusions in their research activities.
%
However analyzing and visualizing large individual software products or a large
number of individual software products is only cost-effective with the
assistance of automated tools.
%% But nao eh recomendado em inicio de frase.

Our research group have been working with empirical studies that require
large-scale source code analysis, and consequently we resort to source
code analysis tools in order to support some of our tasks.
%
We have defined the following requirements for the tool support we needed:

\begin{itemize}
  \item Multi-language.
    The tool should support the analysis of different programming
    languages (in particular, at least C, C++ and Java),  since this can
    enhance the validity of our studies.
  \item Free software.
    The tool should be free software\footnote{In our work, we consider
    the terms ``free software'' and ``open source software''
    equivalent.}, available without restrictions, in order to promote
    the replicability of our studies by other researchers.
  \item Extensibility.
    The tool should provide clear interfaces for adding new types of
    analyzes, metrics, or output formats, in order to promote the
    continuous support to our studies as the research progresses.
\end{itemize}

In this paper, we present Analizo, a toolkit for source code analysis
and visualization, developed with the goal of fulfilling these
requirements.
%
Section \ref{sec:related-work} describes related work.
%
Section \ref{sec:architecture} describes Analizo architecture.
%
Section \ref{sec:features} presents Analizo features.
%
Section \ref{sec:use-cases} presents Analizo use cases.
%
Finally, Section \ref{sec:final-remarkds} concludes the paper and discusses
future work.
%% se eh para colocar ponto-e-virgula, coloco logo ponto. Acho que fica melhor de ler.


\section{Related work}
\label{sec:related-work}

%In this section, we describe related work divided in two different
%sections. Subsection \ref{sec:similar-tools} describes similar tools,
%and subsection \ref{sec:use-cases} describes research papers that were
%already supported by Analizo.

%\subsection{Similar tools}
%\label{sec:similar-tools}

%% achei desnecessaria a divisao.
%% Precisamos ganhar espaco para a introducao.

While evaluating the existing tools to use in our research, we analyzed
the following ones: CCCC~\cite{cccc}, Cscope~\cite{cscope_homepage}, LDX
~\cite{sourceVersusObjectCodeExtraction},
CTAGX~\cite{sourceVersusObjectCodeExtraction}, and
CPPX~\cite{sourceVersusObjectCodeExtraction}. Besides the research
requirements described, we have included two practical requirements:

\begin{itemize}
  \item The tool must be actively maintained. This involves having
    active developers who know the tool architecture and can provide
    updates and defect corrections.
  \item The tool must handle source code that cannot be compiled
    anymore. For example, the code may have syntax errors, the
    libraries it references may be not available anymore, or the
    used libraries changed API. This is important in order to be
    able to analyze legacy source code in software evolution studies.
\end{itemize}

The requirements evaluation for the tools are presented in Table
\ref{tab:tools}.  Since we only looked at tools that were free software, the
table does not have a line for that requirement. 

\begin{table}[htb]
  \centering
  \scalefont{0.95}
  \begin{tabular}{|l|c|c|c|c|c|}
    \hline
    \textbf{Requirement} &
    \textbf{CCCC} &
    \textbf{Cscope} &
    \textbf{LDX} & 
    \textbf{CTAGX} &
    \textbf{CPPX} \\\hline\hline

    % property                  CCCC        Cscope         LDX      CTAGX   CPPX
    Language support           & C++, Java & C            & C, C++ & C     & C, C++ \\\hline
    Extensibility              & No        & No           & No     & No    & No     \\\hline
    Maintained                 & Yes       & Yes          & No     & No    & No     \\\hline
    Handles non-compiling code & Yes       & No           & No     & No    & No     \\\hline
  \end{tabular}
  \caption{Found tools versus posed requirements}
  \label{tab:tools}
  \scalefont{1}
\end{table}

As it can be seen in Table \ref{tab:tools}, none of the existing tools we found
fulfills all of our requirements. In special, none of the tools were able to
analyze source code in all three needed languages, and none of them had
documented extension interfaces that could be used to develop new analysis
types or output formats.

\section{Architecture}
\label{sec:architecture}

% Layered view
Analizo architecture is presented in Figure \ref{fig:architecture},
using a Layered style~\cite{clements2002}. Each layer in the diagram
uses only the services provided by the layers directly below it. 

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=0.4\textwidth]{images/architecture.eps}
  \end{center}
  \caption{Analizo architecture, using the Layered Style~\cite{clements2002}}
  \label{fig:architecture}
\end{figure}

The \emph{Core} layer contains the data structures used to store
information concerning the source code being analyzed, such as the list of
existing modules\footnote{we used the ``module'' concept
as a general term for the different types of structures used in software
development, as classes and C source files}, elements inside each module 
(attributes/variables, or methods/functions), dependency information
(call, inheritance, etc). This layer implements most of Analizo
business logic, and it does not depend on any other layer.

% About extractors
The \emph{Extractors} layer comprises the different source code information
extraction strategies built in Analizo. Extractors get information from
source code and store them in the \emph{Core} layer data structures.
%
It requires only the creation of a new subclass to add a new type of extractor
that interfaces with another external tool or provides its own analysis directly.
% Types of extractor
Currently, there are two extractors. Both are interfaces for external
source code parsing tools:
\begin{itemize}
  \item \emph{Analizo::Extractors::Doxyparse} is an interface for
    Doxyparse, a source code parser for C, C++ and Java developed
    by our group~\cite{JoenioCosta2009}. Doxyparse is based on
    Doxygen\footnote{\url{doxygen.org/}}, a multi-language
    source code documentation system that contains a robust parser.
  \item \emph{Analizo::Extractors::Sloccount} is an interface for David A.
    Wheeler's Sloccount\footnote{\url{dwheeler.com/sloccount/}},
    a tool that calculates the number of effective lines of code.
\end{itemize}

% XXX Doxygen provides the multi-language source code analysis
% \item[Doxygen]
% A documentation system for C++, C, Java, Objective-C, Python, IDL
% (Corba and Microsoft flavors), Fortran, VHDL, PHP, C\#, and to some extent D.
% It can extract the code structure from undocumented source files and can also
% visualize the relations between the various elements by means of include
% dependency graphs, inheritance diagrams, and collaboration diagrams, which are
% all generated automatically.\cite{doxygen_homepage}

% Types of output

%% Juntando os paragrafos para ter uma paragrafo completo e ganhar espaço.
The other intermediate layers are \emph{Metrics} and \emph{Output}.
% Metrics 
The \emph{Metrics} layer processes \emph{Core} data structures in
order to calculate metrics. At the moment, Analizo supports a fair set
of metrics (listed in Section \ref{sec:features}).
% Output
The \emph{Output} layer is responsible for handling different file
formats. Currently, the only output format implemented is the DOT format
for dependency graphs, but adding new formats is simply a matter of
adding new output  handler classes.

% Tools
The \emph{Tools} layer comprises a set of command-line tools that
constitute Analizo interface for both users and higher-level
applications. These tools use services provided by the other layers:
they instantiate the core data structures, one or more extractors,
optionally the metrics processors,  an output format module, and
orchestrate them in order to provide the desired result. Most of the
features described in Section \ref{sec:features} are implemented as
Analizo tools.

Those tools are designed to adhere to the UNIX philosophy: they
accomplish specialized tasks and generate output that is suitable to be
fed as input to other tools, either from Analizo itself or other
external tools.
%
Some of the tools are implemented on top of others instead of explicitly
manipulating Analizo internals, and some are designed to provide output for
external applications such as graph drawing programs or data analysis and
visualization applications.

\section{Features}
\label{sec:features}

\subsection{Multi-language source code analysis}

Currently, Analizo supports source analysis of code written in C, C++
and Java. However, it can be extended to support other languages since
it uses Doxyparse, which is based on Doxygen and thus also supports
several different languages.

\subsection{Metrics}

Analizo reports both project-level metrics, which are calculated for the entire
project, and module-level metrics, which are calculated individually for each
module.
%
On the project-level, Analizo also provides basic descriptive statistics
for each of the module-level metrics: sum, mean, median, mode, standard deviation,
variance, skewness and kurtosis of the distribution, minimum, and maximum value.
%
The following metrics are supported at the time of
writing\footnote{References to literature on each metric were omitted because
of space constraints.}:

\begin{itemize}
  \item
    Project-level metrics:
    Total Coupling Factor, Total Lines of Code, Total number of methods
    per abstract class, Total Number of Modules/Classes, Total number of
    modules/classes with at least one defined attributes, Total number
    of modules/classes with at least one defined method, Total Number of
    Methods.
  \item Module-level metrics:
    Afferent Connections per Class, Average Cyclomatic Complexity per
    Method, Average Method LOC, Average Number of Parameters per Method,
    Coupling Between Objects, Depth of Inheritance Tree, Lack of
    Cohesion of Methods, Lines of Code, Max Method LOC, Number of
    Attributes, Number of Children, Number of Methods, Number of Public
    Attributes, Number of Public Methods, Response For a Class.
\end{itemize}


\subsection{Metrics batch processing}
\label{sec:metrics-batch}

In most quantitative studies on Software Engineering involving the acquisition of
source code metrics on a large number of projects, processing each project
individually is impractical, error-prone and difficult to repeat. 
%
Analizo can process multiple projects in batch and produce one comma-separated 
values (CSV) metrics data file for each project, as well as a summary CSV data
file with project-level metrics for all projects.
%
These data files can be easily imported in statistical tools or in spreadsheet
software for further analysis. This can also be used to analyze several
releases of the same project, in software evolution studies.

\subsection{Metrics history}

Sometimes researchers need to process the history of software projects
on a more fine-grained scale. Analizo can process a version control
repository and provide a CSV data file with the metrics values for each
revision in which source code was changed in the project. Git and
Subversion repositories are supported directly, and CVS repositories
must be converted into Git ones beforehand.

\subsection{Dependency Graph output}

Analizo can output module dependency information extracted from a source
code tree in a format suitable for processing with the
Graphviz\footnote{\url{graphviz.org/}} graph drawing tools.
Figure  \ref{fig:gmpc-graph} presents a sample dependency graph obtained
by feeding Graphviz' \emph{dot} tool with Analizo graph output.

\subsection{Evolution matrix}

Another useful Analizo feature is generating evolution
matrices~\cite{lanza2001}. After processing each release of the project (see
Section \ref{sec:metrics-batch}), the user can request the creation of an
evolution matrix from the individual data files. Figure
\ref{fig:evolution-matrix} shows an excerpt of a sample evolution matrix
produced by Analizo.

\begin{figure}[htbp]
  \centering
  \subfigure[Sample module dependency graph]{\includegraphics[height=0.25\textheight]{images/ristretto.eps}\label{fig:gmpc-graph}}
  \subfigure[Sample evolution matrix]{\includegraphics[height=0.25\textheight]{images/evolution-matrix.eps}\label{fig:evolution-matrix}}
  \caption{Examples of Analizo features.}
\end{figure}

\section{Use cases}
\label{sec:use-cases}

Analizo has been validated in the context of research work performed by
our group that required tool support with at least one of its features:

\begin{itemize}
  \item 
    \cite{vagner_amaral_2009} used Analizo module dependency graph
    output to produce an evolution matrix for a case study on the
    evolution of the VLC project. Later on, an evolution matrix tool was
    incorporated in Analizo itself.
  \item
    \cite{JoenioCosta2009} did a comparison between different strategies
    for extracting module dependency information from source code,
    leading to the development of Doxyparse -- the Analizo Doxygen-based
    extractor.
  \item 
    \cite{structuralComplexityEvolution} used the
    metrics output on an exploratory study on the evolution of
    structural complexity in a free software project written in C.
  \item
    \cite{carlos_morais_2009} used the Analizo metrics tool as a backend for
    Kalibro\footnote{\url{softwarelivre.org/mezuro/kalibro/}}, a
    software metrics evaluation tool. Later on, Kalibro Web
    Service\footnote{\url{ccsl.ime.usp.br/kalibro-service}} was developed,
    providing an integration with
    Spago4Q\footnote{\url{spago4q.org}} -- a free platform
    to measure, analyze and monitor quality of products, processes and services.
  \item
    \cite{terceiro_rios_chavez_2010} used the
    metrics history processing feature to analyze the complete history
    of changes in 7 web server projects of varying sizes.
  \item 
    \cite{meirelles2010} used Analizo metrics
    batch feature to process the source code of more than 6000 free
    software projects from the Sourceforge.net repository.
\end{itemize}

Most of the work cited above contributed to improvements in Analizo,
making it even more appropriate for research involving source code
analysis.

\section{Final remarks}
\label{sec:final-remarkds}

This paper presented Analizo, a toolkit for source code analysis and
visualization that currently supports C, C++ and Java. Analizo has useful
features for both researchers working with source code analysis and
professionals who want to analyze their source code in order to identify
potential problems or possible enhancements.

Future work includes the development of a web-based platform for source code
analysis and visualization based on Analizo. This project is current under
development.

Analizo is free software, licensed under the GNU General Public License
version 3. Its source code, as well as pre-made binary packages, manuals
and tutorials can be obtained from
\url{softwarelivre.org/mezuro/analizo}. All tools are
self-documented and provide on-line manuals.
Analizo is mostly written in Perl, with some of its tools written in
Ruby and Shell Script.

This work is supported by CNPQ, FAPESB, the National Institute of
Science and Technology for Software Engineering (INES), Qualipso project, and
USP FLOSS Competence Center (CCSL-USP).

\bibliographystyle{sbc}
\scalefont{0.95}
\bibliography{bibliography}
\end{document}
